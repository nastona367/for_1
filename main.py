from const import PRINTED, SUITS, RANKS, MESSAGES
import random
import abc
from itertools import product
from random import shuffle
import time

# КАРТИ


class Card:

    def __init__(self, suit, rank, picture, points):
        self.suit = suit
        self.rank = rank
        self.picture = picture
        self.points = points

    def __str__(self):
        message = self.picture + '\nБалів: ' + str(self.points)
        time.sleep(1)
        return message


class Deck:

    def __init__(self):
        self.cards = self._generate_deck()
        shuffle(self.cards)

    def _generate_deck(self):
        cards = []
        for suit, rank in product(SUITS, RANKS):
            if rank == 'туз':
                points = 11
            elif rank.isdigit():
                points = int(rank)
            else:
                points = 10
            picture = PRINTED.get(rank)
            c = Card(suit=suit, rank=rank, points=points, picture=picture)
            cards.append(c)
        return cards

    def get_card(self):
        try:
            return self.cards.pop()
        except IndexError:
            print("Карти закінчилися. Гра завершилась!")
            print("Ваша сума: ", g.player.money)
            exit(1)


# ГРАВЦІ


class AbstractPlayer(abc.ABC):

    def __init__(self):
        self.cards = []
        self.bet = 0
        self.full_points = 0
        self.money = 100

    def change_points(self):
        self.full_points = sum([card.points for card in self.cards])

    def take_card(self, card):
        self.cards.append(card)
        self.change_points()

    @abc.abstractmethod
    def change_bet(self,  max_bet, min_bet):
        pass

    @abc.abstractmethod
    def ask_card(self):
        pass

    def print_cards(self):
        print('----------------------')
        print(self, " data")
        for card in self.cards:
            print(card)
        print('Всього балів: ', self.full_points)


class Player(AbstractPlayer):
    def change_bet(self, max_bet, min_bet):
        while True:
            while True:
                try:
                    value = int(input('Введіть вашу ставку: '))
                except ValueError:
                    print("Хибна відповідь!")
                else:
                    break
            if min_bet < value < max_bet:
                self.bet = value
                self.money -= self.bet
                break
        print('Ваша ставка: ', self.bet)

    def ask_card(self):
        choice = input(MESSAGES.get('ask_card'))
        if choice == 'так':
            return True
        else:
            return False


class Bot(AbstractPlayer):
    def __init__(self):
        super().__init__()
        self.max_points = random.randint(17, 20)

    def change_bet(self, max_bet, min_bet):
        self.bet = random.randint(min_bet, max_bet)
        self.money -= self.bet
        print(self, 'має: ', self.bet)
        time.sleep(1)

    def ask_card(self):
        if self.full_points < self.max_points:
            return True
        else:
            return False


class Dealer(AbstractPlayer):
    max_points = 17

    def change_bet(self, max_bet, min_bet):
        raise Exception('Це дилер, він не має позиції!')

    def ask_card(self):
        if self.full_points < self.max_points:
            return True
        else:
            return False

# САМА ГРА


class Game:
    max_pl_count = 4

    def __init__(self):
        self.players = []
        self.player = None
        self.player_pos = None
        self.dealer = Dealer()
        self.all_players_count = 1
        self.deck = Deck()
        self.max_bet, self.min_bet = 20, 0

    @staticmethod
    def _ask_starting(message):
        while True:
            choice = input(message)
            if choice == 'ні':
                print("Вихід з програми!")
                return False
            elif choice == 'так':
                return True
            else:
                print("Хибна відповідь!")

    def _launching(self):
        while True:
            while True:
                try:
                    bots_count = int(input('Напишуть потрібну '
                                           'кількість гравців (не більше 3): '))
                except ValueError:
                    print("Хибна відповідь!")
                else:
                    break
            if bots_count <= self.max_pl_count - 1:
                break
            else:
                print("Не вірна кількість гравців!")
        self.all_players_count = bots_count + 1
        for a in range(bots_count):
            b = Bot()
            self.players.append(b)
        self.player = Player()
        self.player_pos = random.randint(0, self.all_players_count)
        print('Ваша позиція:', self.player_pos)
        self.players.insert(self.player_pos, self.player)

    def ask_bet(self):
        for player in self.players:
            player.change_bet(self.max_bet, self.min_bet)

    def first_descr(self):
        for player in self.players:
            for _ in range(2):
                card = self.deck.get_card()
                player.take_card(card)
        card = self.deck.get_card()
        self.dealer.take_card(card)
        self.dealer.print_cards()

    def check_stop(self, player):
        if player.full_points >= 21:
            return True
        else:
            return False

    def remove_player(self, player):
        player.print_cards()
        if isinstance(player, Player):
            print('Ви програли!')
        elif isinstance(player, Bot):
            print(player, 'програв/ла!')
        self.players.remove(player)

    def ask_cards(self):
        for player in self.players:
            while player.ask_card():
                card = self.deck.get_card()
                player.take_card(card)
                is_stop = self.check_stop(player)
                if is_stop:
                    if player.full_points > 21 or isinstance(player, Player):
                        self.remove_player(player)
                    break
                if isinstance(player, Player):
                    player.print_cards()

    def check_winner(self):
        if self.dealer.full_points > 21:
            print('Ділер програв! Всі гравці перемогли!')
            for winner in self.players:
                winner.money += winner.bet * 2
        else:
            for player in self.players:
                if player.full_points == self.dealer.full_points:
                    player.money += player.bet
                    print(MESSAGES.get('eq').format(player=player,
                                                    points=player.full_points))
                elif player.full_points > self.dealer.full_points:
                    player.money += player.bet * 2
                    if isinstance(player, Bot):
                        print(MESSAGES.get('win').format(player))
                    elif isinstance(player, Player):
                        print('Ти переміг!')

                elif player.full_points < self.dealer.full_points:
                    if isinstance(player, Bot):
                        print(MESSAGES.get('lose').format(player))
                    elif isinstance(player, Player):
                        print('Ти програв!')

    def play_with_dealer(self):
        while self.dealer.ask_card():
            card = self.deck.get_card()
            self.dealer.take_card(card)
        self.dealer.print_cards()

    def start_game(self):
        message = MESSAGES.get('ask_start')
        if not self._ask_starting(message=message):
            exit(1)
        self._launching()
        while True:
            self.ask_bet()
            self.first_descr()
            self.player.print_cards()
            self.ask_cards()
            self.play_with_dealer()
            self.check_winner()
            if not self._ask_starting(MESSAGES.get('rerun')):
                break


if __name__ == '__main__':
    g = Game()
    g.start_game()
    print("Ваша сума: ", g.player.money)
